package shock

import (
	"testing"
)

var tt = []*TestUser{
	{
		Name:  "user1",
		Age:   25,
		Admin: true,
	},
	{
		Name:  "user2",
		Age:   30,
		Admin: false,
	},
	{
		Name:  "user3",
		Age:   40,
		Admin: false,
	},
}

func TestStore(t *testing.T) {

	ttt := []*TestUserSettings{
		{
			UserID: 1,
			Theme:  "light",
		},
		{
			UserID: 2,
			Theme:  "dark",
		},
	}

	bucket := testDB.Bucket("store")
	if err := bucket.Init(); err != nil {
		t.Log(err)
		t.FailNow()
	}

	testStore(t, bucket)

	t.Run("count", func(t *testing.T) {
		count, err := bucket.Count()
		if err != nil {
			t.Log(err)
			t.FailNow()
		}

		if count != len(tt) {
			t.Log("Count is off")
			t.FailNow()
		}
	})

	t.Run("list", func(t *testing.T) {
		list := make([]TestUser, 0, len(tt))
		if err := bucket.ViewEach(func(_, serial []byte) error {
			var t TestUser
			if err := testDB.Serializer.Unmarshal(serial, &t); err != nil {
				return err
			}
			list = append(list, t)
			return nil
		}); err != nil {
			t.Log(err)
			t.FailNow()
		}

		for idx, l := range list {
			if !tt[idx].Equal(l) {
				t.Logf("List doesn't match:\n%v\n%v\n", tt[idx], l)
				t.FailNow()
			}
		}
	})

	for _, tc := range ttt {
		t.Run(tc.Theme, func(t *testing.T) {
			if err := bucket.PutWithKey(tc.UserID, tc); err != nil {
				t.Log(err)
				t.FailNow()
			}

			var tcc TestUserSettings
			err := bucket.Get(tc.UserID, &tcc)
			if err != nil {
				t.Log(err)
				t.FailNow()
			}

			if !tcc.Equal(*tc) {
				t.Log("Serialized struct is not the same")
				t.FailNow()
			}
		})
	}
}

func testStore(t *testing.T, bucket *Bucket) {
	for _, tc := range tt {
		t.Run(tc.Name, func(t *testing.T) {
			if err := bucket.Put(tc); err != nil {
				t.Log(err)
				t.FailNow()
			}

			var tcc TestUser
			err := bucket.Get(tc.ID, &tcc)
			if err != nil {
				t.Log(err)
				t.FailNow()
			}

			if !tcc.Equal(*tc) {
				t.Log("Serialized struct is not the same")
				t.FailNow()
			}
		})
	}
}

func (t TestUser) Equal(tt TestUser) bool {
	return t.ID == tt.ID && t.Name == tt.Name &&
		t.Age == tt.Age && t.Admin == tt.Admin
}

func (t TestUserSettings) Equal(tt TestUserSettings) bool {
	return t.UserID == tt.UserID && t.Theme == tt.Theme
}

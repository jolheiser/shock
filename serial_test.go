package shock

import (
	"fmt"
	"os"
	"path"
	"testing"

	"go.etcd.io/bbolt"
)

func TestGob(t *testing.T) {
	dbPath := path.Join(tmpDir, "gob.db")
	gobDB, err := Open(dbPath, os.ModePerm, &Options{
		Bolt:       bbolt.DefaultOptions,
		Serializer: GobSerializer{},
	})
	if err != nil {
		panic(err)
	}

	bucket := gobDB.Bucket("gob")
	if err := bucket.Init(); err != nil {
		t.Log(err)
		t.FailNow()
	}

	testStore(t, bucket)

	if err := gobDB.Bolt.Close(); err != nil {
		fmt.Printf("Could not close DB %s: %v\n", dbPath, err)
	}
}

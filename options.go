package shock

import "go.etcd.io/bbolt"

// Options are Shock init options
type Options struct {
	Bolt       *bbolt.Options
	Serializer Serializer
}

// DefaultOptions are the default options used to init Shock
var DefaultOptions = &Options{
	Bolt:       bbolt.DefaultOptions,
	Serializer: JSONSerializer{},
}

package shock

import (
	"fmt"
	"os"
	"path"
)

// TestUser object
type TestUser struct {
	ID    uint64 `json:"id"`
	Name  string `json:"name"`
	Age   int    `json:"age"`
	Admin bool   `json:"admin"`
}

// Implement the interface for Sequencer
func (t *TestUser) AssignSeq(seq uint64) {
	t.ID = seq
}

func ExampleTestUser() {
	dbPath := path.Join(tmpDir, "seq.db")
	seqDB, err := Open(dbPath, os.ModePerm, DefaultOptions)
	if err != nil {
		panic(err)
	}

	// Get a bucket to work with instead of specifying each time
	bucket := seqDB.Bucket("test-user")
	if err := bucket.Init(); err != nil {
		panic(err)
	}

	// Add a new TestUser
	t := &TestUser{
		Name:  "Tester123",
		Age:   25,
		Admin: true,
	}
	if err := bucket.Put(t); err != nil {
		panic(err)
	}

	// Get the TestUser from the DB later (via sequence ID)
	var tt TestUser
	if err := bucket.Get(t.ID, &tt); err != nil {
		panic(err)
	}

	if err := seqDB.Bolt.Close(); err != nil {
		fmt.Printf("Could not close DB %s: %v\n", dbPath, err)
	}
}

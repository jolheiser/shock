GO ?= go

.PHONY: fmt
fmt:
	$(GO) fmt ./...

.PHONY: test
test:
	$(GO) test --race ./...

.PHONY: vet
vet:
	$(GO) vet ./...

.PHONY: coverage
coverage:
	$(GO) test --coverprofile=coverage.out
	$(GO) tool cover --html=coverage.out
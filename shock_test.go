package shock

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"testing"
)

var (
	testDB *DB
	tmpDir string
)

func TestMain(m *testing.M) {
	var err error
	tmpDir, err = ioutil.TempDir(os.TempDir(), "shock")
	if err != nil {
		panic(err)
	}

	dbPath := path.Join(tmpDir, "shock.db")
	testDB, err = Open(dbPath, os.ModePerm, nil)
	if err != nil {
		panic(err)
	}

	exit := m.Run()

	if err := testDB.Bolt.Close(); err != nil {
		fmt.Printf("Could not close DB %s: %v\n", dbPath, err)
	}
	if err := os.RemoveAll(tmpDir); err != nil {
		fmt.Printf("Could not delete temp dir %s: %v\n", tmpDir, err)
	}

	os.Exit(exit)
}

package shock

import (
	"fmt"
	"os"
	"path"
)

// TestUserSettings object
type TestUserSettings struct {
	UserID uint64 `json:"user_id"`
	Theme  string `json:"theme"`
}

func ExampleTestUserSettings() {
	dbPath := path.Join(tmpDir, "key.db")
	keyDB, err := Open(dbPath, os.ModePerm, DefaultOptions)
	if err != nil {
		panic(err)
	}

	// Get a bucket to work with instead of specifying each time
	bucket := keyDB.Bucket("test-user-settings")
	if err := bucket.Init(); err != nil {
		panic(err)
	}

	// Add a new TestUserSettings
	t := &TestUserSettings{
		UserID: 1,
		Theme:  "dark",
	}
	if err := bucket.PutWithKey(t.UserID, t); err != nil {
		panic(err)
	}

	// Get the TestUser from the DB later (via sequence ID)
	var tt TestUserSettings
	if err := bucket.Get(t.UserID, &tt); err != nil {
		panic(err)
	}

	if err := keyDB.Bolt.Close(); err != nil {
		fmt.Printf("Could not close DB %s: %v\n", dbPath, err)
	}
}

package shock

import (
	"os"

	"go.etcd.io/bbolt"
)

// DB is a Shock (BBolt) database
type DB struct {
	Bolt       *bbolt.DB
	Serializer Serializer
}

// Open creates a connection to a database file
func Open(path string, mode os.FileMode, options *Options) (*DB, error) {
	if options == nil {
		options = DefaultOptions
	}
	db, err := bbolt.Open(path, mode, options.Bolt)
	if err != nil {
		return nil, err
	}
	return &DB{
		Bolt:       db,
		Serializer: options.Serializer,
	}, nil
}

// Bucket returns a shorthand wrapper for interacting with a specific bucket
func (d *DB) Bucket(name string) *Bucket {
	return &Bucket{
		Name: name,
		DB:   d,
	}
}

// Init initializes buckets if they don't exist
func (d *DB) Init(buckets ...string) error {
	return d.Bolt.Update(func(tx *bbolt.Tx) error {
		for _, bucket := range buckets {
			if _, err := tx.CreateBucketIfNotExists([]byte(bucket)); err != nil {
				return err
			}
		}
		return nil
	})
}

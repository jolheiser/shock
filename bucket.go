package shock

// Bucket is a shorthand way to group a database bucket together
type Bucket struct {
	Name string
	DB   *DB
}

// Put adds a new value to the bucket
func (b *Bucket) Put(val Sequencer) error {
	return b.DB.Put(b.Name, val)
}

// PutWithKey adds a new value with the specified key to the bucket
func (b *Bucket) PutWithKey(key, val interface{}) error {
	return b.DB.PutWithKey(b.Name, key, val)
}

// Get returns a value from the bucket with the specified sequence ID
func (b *Bucket) Get(id, val interface{}) error {
	return b.DB.Get(b.Name, id, val)
}

// Count returns the number of objects in the bucket
func (b *Bucket) Count() (int, error) {
	return b.DB.Count(b.Name)
}

// ViewEach iterates over each object in the bucket in read-only mode
func (b *Bucket) ViewEach(fn EachFunc) error {
	return b.DB.ViewEach(b.Name, fn)
}

// UpdateEach iterates over each object in the bucket in write mode
func (b *Bucket) UpdateEach(fn EachFunc) error {
	return b.DB.UpdateEach(b.Name, fn)
}

// Init initializes the bucket if it doesn't exist
func (b *Bucket) Init() error {
	return b.DB.Init(b.Name)
}

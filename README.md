# Shock

A wrapper around BBolt.

Check out the [docs](https://pkg.go.dev/go.jolheiser.com/shock) or 
[an](example_key_test.go) [example](example_seq_test.go).

## License

[MIT](LICENSE)